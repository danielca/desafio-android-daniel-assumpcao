package br.com.danielassumpcao.marvelreader.Listeners;

import br.com.danielassumpcao.marvelreader.Models.Comic;

public interface ComicsListener {
    void onComicSucess(Comic comic);
    void onComicFail();
}
