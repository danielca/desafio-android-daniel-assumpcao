package br.com.danielassumpcao.marvelreader.Presenters;

import com.himanshurawat.hasher.HashType;
import com.himanshurawat.hasher.Hasher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.danielassumpcao.marvelreader.BuildConfig;
import br.com.danielassumpcao.marvelreader.Listeners.AllCharactersListener;
import br.com.danielassumpcao.marvelreader.Listeners.ComicsListener;
import br.com.danielassumpcao.marvelreader.Models.Character;
import br.com.danielassumpcao.marvelreader.Models.Comic;
import br.com.danielassumpcao.marvelreader.Models.DataContainer;
import br.com.danielassumpcao.marvelreader.Models.DataWrapper;
import br.com.danielassumpcao.marvelreader.Models.Price;
import br.com.danielassumpcao.marvelreader.Services.MarvelService;
import br.com.danielassumpcao.marvelreader.Services.RetrofitConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarvelPresenter {

    final static int STD_OFFSET = 20;
    MarvelService service;
    List<Comic> allComics;
    boolean cancelRecursion;

    public MarvelPresenter() {
        this.service = new RetrofitConfig().getMarvelService();
        this.allComics = new ArrayList<>();
        this.cancelRecursion = false;
    }

    private String generateHash(Long timeStamp) {
        String apiRawHash = timeStamp + BuildConfig.PRIVATE_KEY + BuildConfig.PUBLIC_KEY;
        return Hasher.Companion.hash(apiRawHash, HashType.MD5);
    }

    public void getAllCharacters(int offset, AllCharactersListener listener) {
        Long timeStamp = new Date().getTime();
        String apiHash = generateHash(timeStamp);
        Call<DataWrapper<Character>> call = service.getAllCharacters(BuildConfig.PUBLIC_KEY, timeStamp, apiHash, offset);

        call.enqueue(new Callback<DataWrapper<Character>>() {
            @Override
            public void onResponse(Call<DataWrapper<Character>> call, Response<DataWrapper<Character>> response) {
                if (response != null && response.body() != null) {
                    listener.onAllCharactersSucess(response.body().getData().getResults(), response.body().getData().getTotal());
                } else {
                    listener.onAllCharactersFail();
                }
            }

            @Override
            public void onFailure(Call<DataWrapper<Character>> call, Throwable t) {
                listener.onAllCharactersFail();
            }
        });
    }

    public Comic findExpensiveComic(List<Comic> allComics) {
        if(allComics.size() > 0) {
            Comic expensive = allComics.get(0);
            Float biggerPrice = expensive.getPrices().get(0).getPrice();
            for (Comic comic : allComics) {
                for (Price price : comic.getPrices()) {
                    if (price.getPrice() > biggerPrice) {
                        expensive = comic;
                        biggerPrice = price.getPrice();
                    }
                }
            }

            return expensive;
        }else{
            return null;
        }
    }

    public void cancelRequest(){
        cancelRecursion = true;
    }

    public void getExpensiveComic(int characterId, ComicsListener listener) {
        getPageComic(characterId, 0, listener);
    }

    void getPageComic(int characterId, int offset, ComicsListener listener){
        Long timeStamp = new Date().getTime();
        String apiHash = generateHash(timeStamp);
        Call<DataWrapper<Comic>> call = service.getComics(characterId, BuildConfig.PUBLIC_KEY, timeStamp, apiHash, offset);
        call.enqueue(new Callback<DataWrapper<Comic>>() {
            @Override
            public void onResponse(Call<DataWrapper<Comic>> call, Response<DataWrapper<Comic>> response) {
                if (response != null && response.body() != null) {
                    DataContainer<Comic> container = response.body().getData();
                    allComics.addAll(container.getResults());
                    if (allComics.size() >= container.getTotal()) {
                        listener.onComicSucess(findExpensiveComic(allComics));
                    } else {
                        if(!cancelRecursion){
                            getPageComic(characterId, offset + STD_OFFSET, listener);
                        }
                    }
                } else {
                    listener.onComicFail();
                }
            }

            @Override
            public void onFailure(Call<DataWrapper<Comic>> call, Throwable t) {
                listener.onComicFail();
            }
        });
    }
}
