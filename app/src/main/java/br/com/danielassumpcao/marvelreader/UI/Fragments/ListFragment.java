package br.com.danielassumpcao.marvelreader.UI.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import br.com.danielassumpcao.marvelreader.Listeners.AllCharactersListener;
import br.com.danielassumpcao.marvelreader.Listeners.CharacterClickListener;
import br.com.danielassumpcao.marvelreader.Models.Character;
import br.com.danielassumpcao.marvelreader.Presenters.MarvelPresenter;
import br.com.danielassumpcao.marvelreader.R;
import br.com.danielassumpcao.marvelreader.UI.Activities.MainActivity;
import br.com.danielassumpcao.marvelreader.UI.Adapters.AllCharactersAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListFragment extends Fragment implements AllCharactersListener, CharacterClickListener {

    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;

    @BindView(R.id.recycleView)
    RecyclerView recycleView;

    @BindView(R.id.emptyTV)
    TextView emptyTV;

    MarvelPresenter presenter;
    AllCharactersAdapter adapter;
    MainActivity activity;

    final static int PAGE_SIZE = 20;
    int offset = 0;
    int totalItens = 99;
    boolean isLoading = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        presenter = new MarvelPresenter();

        if (getActivity() != null) {
            activity = (MainActivity) getActivity();
            activity.showBackButton(false);
            if (activity.getAllCharacters() == null) {
                activity.setAllCharacters(new ArrayList<>());
                emptyTV.setVisibility(View.VISIBLE);
            } else {
                emptyTV.setVisibility(View.GONE);
            }
        }
        setupViews();
    }

    void setupViews() {
        swipeLayout.setOnRefreshListener(() -> {
            offset = 0;
            activity.getAllCharacters().clear();
            loadItens();
        });

        adapter = new AllCharactersAdapter(getContext(), activity.getAllCharacters(), this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(adapter);

        recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == activity.getAllCharacters().size() - 1) {
                        isLoading = true;
                        loadItens();
                    }
                }

            }
        });
    }

    void loadItens() {
        if (activity.getAllCharacters().size() < totalItens) {
            if(activity.getAllCharacters().size() > 0 && isLoading){
                activity.getAllCharacters().add(null);
                adapter.notifyItemInserted(activity.getAllCharacters().size() - 1);
                recycleView.scrollToPosition(activity.getAllCharacters().size() - 1);
            }else{
                swipeLayout.setRefreshing(true);
            }

            presenter.getAllCharacters(offset, this);
            offset += PAGE_SIZE;
        }
    }

    void stopMiddleScreenLoading(){
        if (activity.getAllCharacters().size() > 0) {
            activity.getAllCharacters().remove(activity.getAllCharacters().size() - 1);
            int scrollPosition = activity.getAllCharacters().size();
            adapter.notifyItemRemoved(scrollPosition);
        }
        isLoading = false;
    }

    @Override
    public void onAllCharactersSucess(List<Character> characters, int totalItens) {
        emptyTV.setVisibility(View.GONE);
        this.totalItens = totalItens;
        stopMiddleScreenLoading();
        activity.getAllCharacters().addAll(characters);
        adapter.notifyDataSetChanged();
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onAllCharactersFail() {
        swipeLayout.setRefreshing(false);
        stopMiddleScreenLoading();
        Snackbar.make(swipeLayout, R.string.error_message, Snackbar.LENGTH_INDEFINITE).setAction(R.string.try_again, (view) -> {
            offset -= PAGE_SIZE;
            loadItens();
        }).show();
    }

    @Override
    public void onCharacterListener(Character selectedCharacter) {
        activity.setSelectedCharacter(selectedCharacter);
        NavHostFragment.findNavController(ListFragment.this)
                .navigate(R.id.action_ListFragment_to_DetailFragment);
    }
}