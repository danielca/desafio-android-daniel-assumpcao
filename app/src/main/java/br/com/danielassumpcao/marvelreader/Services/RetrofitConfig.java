package br.com.danielassumpcao.marvelreader.Services;

import br.com.danielassumpcao.marvelreader.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitConfig {
    private final Retrofit retrofit;

    public RetrofitConfig() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.HOST_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    public MarvelService getMarvelService(){
        return this.retrofit.create(MarvelService.class);
    }

}