package br.com.danielassumpcao.marvelreader.Listeners;

import br.com.danielassumpcao.marvelreader.Models.Character;

public interface CharacterClickListener {
    void onCharacterListener(Character selectedCharacter);
}
