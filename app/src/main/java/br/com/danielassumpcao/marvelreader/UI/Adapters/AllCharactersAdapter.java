package br.com.danielassumpcao.marvelreader.UI.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.danielassumpcao.marvelreader.Listeners.CharacterClickListener;
import br.com.danielassumpcao.marvelreader.Models.Character;
import br.com.danielassumpcao.marvelreader.R;

public class AllCharactersAdapter extends RecyclerView.Adapter {

    Context context;
    List<Character> allCharacters;
    CharacterClickListener listener;

    public AllCharactersAdapter(Context context, List<Character> allCharacters, CharacterClickListener listener) {
        this.context = context;
        this.allCharacters = allCharacters;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_character, parent, false);
        CharacterViewHolder viewHolder = new CharacterViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Character selectedCharacter = allCharacters.get(position);
        CharacterViewHolder viewHolder = (CharacterViewHolder) holder;

        if(selectedCharacter == null){
            viewHolder.loadingLL.setVisibility(View.VISIBLE);
            viewHolder.container.setVisibility(View.GONE);
        }
        else {
            viewHolder.loadingLL.setVisibility(View.GONE);
            viewHolder.container.setVisibility(View.VISIBLE);

            viewHolder.characterTV.setText(selectedCharacter.getName());

            Picasso.get()
                    .load(selectedCharacter.getThumbnail().getFullPath())
                    .placeholder(R.mipmap.ic_launcher)
                    .fit()
                    .into(viewHolder.thumbnailIV);

            viewHolder.container.setOnClickListener(view -> {
                listener.onCharacterListener(selectedCharacter);
            });
        }


    }

    @Override
    public int getItemCount() {
        if (allCharacters != null) {
            return allCharacters.size();
        }
        return 0;
    }


    public static class CharacterViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout container;
        public ImageView thumbnailIV;
        public TextView characterTV;
        public LinearLayout loadingLL;



        public CharacterViewHolder(View view) {
            super(view);
            container = view.findViewById(R.id.container);
            thumbnailIV = view.findViewById(R.id.thumbnailIV);
            characterTV = view.findViewById(R.id.characterTV);
            loadingLL = itemView.findViewById(R.id.loadingLL);



        }
    }
}


