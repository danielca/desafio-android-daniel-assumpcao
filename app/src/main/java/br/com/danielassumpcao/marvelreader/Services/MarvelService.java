package br.com.danielassumpcao.marvelreader.Services;

import br.com.danielassumpcao.marvelreader.Models.Character;
import br.com.danielassumpcao.marvelreader.Models.Comic;
import br.com.danielassumpcao.marvelreader.Models.DataWrapper;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MarvelService {
    @GET("v1/public/characters")
    Call<DataWrapper<Character>> getAllCharacters(@Query("apikey") String apiKey,
                                                  @Query("ts") Long timestamp,
                                                  @Query("hash") String hash,
                                                  @Query("offset") int offset);

    @GET("/v1/public/characters/{characterId}/comics")
    Call<DataWrapper<Comic>> getComics(@Path("characterId") int characterId,
                                       @Query("apikey") String apiKey,
                                       @Query("ts") Long timestamp,
                                       @Query("hash") String hash,
                                       @Query("offset") int offset
                                       );
}
