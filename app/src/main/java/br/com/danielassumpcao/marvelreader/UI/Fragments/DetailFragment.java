package br.com.danielassumpcao.marvelreader.UI.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import br.com.danielassumpcao.marvelreader.Models.Character;
import br.com.danielassumpcao.marvelreader.R;
import br.com.danielassumpcao.marvelreader.UI.Activities.MainActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailFragment extends Fragment {


    @BindView(R.id.thumbnailIV)
    ImageView thumbnailIV;

    @BindView(R.id.comicFloating)
    FloatingActionButton comicFloating;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;

    MainActivity activity;
    Character selectedCharacter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            activity = (MainActivity) getActivity();
            selectedCharacter = activity.getSelectedCharacter();
            activity.showBackButton(true);
        }
        setupView();

    }

    void setupView() {
        nameTV.setText(selectedCharacter.getName());
        if (selectedCharacter.getDescription().equals("")) {
            descriptionTV.setText(R.string.missing_description);
        } else {
            descriptionTV.setText(selectedCharacter.getDescription());
        }
        Picasso.get()
                .load(selectedCharacter.getThumbnail().getFullPath())
                .placeholder(R.mipmap.ic_launcher)
                .into(thumbnailIV);

        comicFloating.setOnClickListener(view -> {
            NavHostFragment.findNavController(DetailFragment.this)
                    .navigate(R.id.action_DetailFragment_to_ComicFragment);
        });
    }
}