package br.com.danielassumpcao.marvelreader.UI.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import br.com.danielassumpcao.marvelreader.Listeners.ComicsListener;
import br.com.danielassumpcao.marvelreader.Models.Character;
import br.com.danielassumpcao.marvelreader.Models.Comic;
import br.com.danielassumpcao.marvelreader.Models.Price;
import br.com.danielassumpcao.marvelreader.Presenters.MarvelPresenter;
import br.com.danielassumpcao.marvelreader.R;
import br.com.danielassumpcao.marvelreader.UI.Activities.MainActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ComicFragment extends Fragment implements ComicsListener {

    @BindView(R.id.thumbnailIV)
    ImageView thumbnailIV;
    @BindView(R.id.titleTV)
    TextView titleTV;
    @BindView(R.id.priceTV)
    TextView priceTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    MarvelPresenter presenter;
    MainActivity activity;
    Character selectedCharacter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comic, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            activity = (MainActivity) getActivity();
            selectedCharacter = activity.getSelectedCharacter();
            activity.showBackButton(true);
        }
        presenter = new MarvelPresenter();

        if (selectedCharacter.getExpensiveComic() == null) {
            startLoading();
            presenter.getExpensiveComic(selectedCharacter.getId(), this);
        } else {
            setupView();
        }
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.cancelRequest();
    }

    void setupView() {
        Comic expensiveComic = selectedCharacter.getExpensiveComic();
        if(expensiveComic != null){
            titleTV.setText(expensiveComic.getTitle());
            descriptionTV.setText(expensiveComic.getDescription());
            Float biggerPrice = 0f;

            for (Price price : expensiveComic.getPrices()) {
                if (price.getPrice() > biggerPrice) {
                    biggerPrice = price.getPrice();
                }
            }

            priceTV.setText(getString(R.string.price, biggerPrice));
            Picasso.get()
                    .load(expensiveComic.getThumbnail().getFullPath())
                    .placeholder(R.mipmap.ic_launcher)
                    .into(thumbnailIV);
            stopLoading();

        }else{
            progressBar.setVisibility(View.GONE);
            Snackbar.make(getView(), R.string.empty_comic, Snackbar.LENGTH_INDEFINITE).setAction(R.string.back, (view) -> {
                activity.onBackPressed();
            }).show();
        }

    }



    void startLoading() {
        thumbnailIV.setVisibility(View.GONE);
        titleTV.setVisibility(View.GONE);
        priceTV.setVisibility(View.GONE);
        descriptionTV.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    void stopLoading() {
        thumbnailIV.setVisibility(View.VISIBLE);
        titleTV.setVisibility(View.VISIBLE);
        priceTV.setVisibility(View.VISIBLE);
        descriptionTV.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onComicSucess(Comic comic) {
        selectedCharacter.setExpensiveComic(comic);
        if(isVisible()){
            setupView();
        }
    }

    @Override
    public void onComicFail() {
        progressBar.setVisibility(View.GONE);
        Snackbar.make(getView(), R.string.error_message, Snackbar.LENGTH_INDEFINITE).setAction(R.string.try_again, (view) -> {
            startLoading();
            presenter.getExpensiveComic(selectedCharacter.getId(), this);
        }).show();
    }
}
