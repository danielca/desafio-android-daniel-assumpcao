package br.com.danielassumpcao.marvelreader.Listeners;

import java.util.List;

import br.com.danielassumpcao.marvelreader.Models.Character;

public interface AllCharactersListener {
    void onAllCharactersSucess(List<Character> characters, int totalItens);
    void onAllCharactersFail();
}
