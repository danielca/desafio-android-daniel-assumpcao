package br.com.danielassumpcao.marvelreader.UI.Activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import br.com.danielassumpcao.marvelreader.Models.Character;
import br.com.danielassumpcao.marvelreader.R;

public class MainActivity extends AppCompatActivity {

    Character selectedCharacter;
    ArrayList<Character> allCharacters;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void showBackButton(boolean show){
        getSupportActionBar().setDisplayHomeAsUpEnabled(show);
        getSupportActionBar().setDisplayShowHomeEnabled(show);
    }



    public Character getSelectedCharacter() {
        return selectedCharacter;
    }

    public void setSelectedCharacter(Character selectedCharacter) {
        this.selectedCharacter = selectedCharacter;
    }

    public ArrayList<Character> getAllCharacters() {
        return allCharacters;
    }

    public void setAllCharacters(ArrayList<Character> allCharacters) {
        this.allCharacters = allCharacters;
    }
}