package br.com.danielassumpcao.marvelreader.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataWrapper<T> {

    @JsonProperty("data")
    private DataContainer<T> data;

    public DataContainer<T> getData() {
        return data;
    }

    public void setData(DataContainer<T> data) {
        this.data = data;
    }
}
