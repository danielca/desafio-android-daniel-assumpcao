package br.com.danielassumpcao.marvelreader;

import org.junit.Test;

import java.util.ArrayList;

import br.com.danielassumpcao.marvelreader.Models.Comic;
import br.com.danielassumpcao.marvelreader.Models.Price;
import br.com.danielassumpcao.marvelreader.Presenters.MarvelPresenter;

import static org.junit.Assert.assertEquals;

public class UnitTest {

    @Test
    public void findExpensiveComic_isCorrect(){

        MarvelPresenter presenter = new MarvelPresenter();
        Comic firstComic = new Comic();
        Price biggerPrice = new Price();
        biggerPrice.setPrice(9.99f);
        ArrayList<Price> biggerPrices = new ArrayList<>();
        biggerPrices.add(biggerPrice);
        firstComic.setPrices(biggerPrices);

        Comic secondComic = new Comic();
        Price smallerPrice = new Price();
        smallerPrice.setPrice(0.99f);
        ArrayList<Price> smallerPrices = new ArrayList<>();
        smallerPrices.add(smallerPrice);
        secondComic.setPrices(smallerPrices);

        ArrayList<Comic> comics = new ArrayList<>();
        comics.add(firstComic);
        comics.add(secondComic);

        assertEquals(firstComic, presenter.findExpensiveComic(comics));

    }
}